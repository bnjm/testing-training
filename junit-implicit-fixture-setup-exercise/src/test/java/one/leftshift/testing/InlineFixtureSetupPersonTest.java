package one.leftshift.testing;

import one.leftshift.testing.common.model.*;
import one.leftshift.testing.common.util.Tuple;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * TODO: Refactor from inline fixture setup to implicit fixture
 */
class InlineFixtureSetupPersonTest {

    @Test
    @DisplayName("A single product can be bought with a valid credit card")
    void testCanBuyProduct() {
        // setup
        //@formatter:off
        Person person = Person.builder()
                .withName()
                    .firstName("Joshua").lastName("Bloch")
                .withAddress()
                    .street("Downing Street 32").town("London").zipCode(2838)
                .withAge()
                    .dateOfBirth(LocalDate.of(1988, 10, 4))
                .build();
        //@formatter:on
        ProductShop classUnderTest = new ProductShop();

        // exercise
        Tuple<List<Product>, Payment> result = classUnderTest.buy(1,
                new CreditCard(person, new BigDecimal("1000")));

        // verify
        Assertions.assertThat(result)
                .extracting(Tuple::getLeft)
                .as("Checking size of product list")
                .hasSize(1);
    }

    @Test
    @DisplayName("Throws Exception if trying to buy -2 items")
    void testThrowsExceptionIfMoreThan100ItemsAreBought() {
        // setup
        //@formatter:off
        Person person = Person.builder()
                .withName()
                    .firstName("Joshua").lastName("Bloch")
                .withAddress()
                    .street("Downing Street 32").town("London").zipCode(2838)
                .withAge()
                    .dateOfBirth(LocalDate.of(1988, 10, 4))
                .build();
        //@formatter:on
        ProductShop classUnderTest = new ProductShop();

        // exercise and verify
        Assertions.assertThatExceptionOfType(ProductShop.InvalidQuantityException.class)
                .isThrownBy(() -> classUnderTest.buy(-2, new CreditCard(person, new BigDecimal("100000"))));
    }

    @Test
    @DisplayName("Throws exception if price surpasses creditcard limit")
    void testThrowsExceptionIfPriceSurpassesCreditcardLimit() {
        // setup
        //@formatter:off
        Person person = Person.builder()
                .withName()
                    .firstName("Joshua").lastName("Bloch")
                .withAddress()
                    .street("Downing Street 32").town("London").zipCode(2838)
                .withAge()
                    .dateOfBirth(LocalDate.of(1988, 10, 4))
                .build();
        //@formatter:on
        ProductShop classUnderTest = new ProductShop();

        // exercise and verify
        Assertions.assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(() -> classUnderTest.buy(100, new CreditCard(person, new BigDecimal("1"))));
    }
}
