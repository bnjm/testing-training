package one.leftshift.testing;

import one.leftshift.testing.common.repository.PersonRepository;
import one.leftshift.testing.common.service.PersonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * TODO: Implement a Mock for this test
 * @author benjamin.krenn@leftshift.one
 * @since 1.0.0
 */
public class MockExerciseTest {

    private PersonService classUnderTest;
    private PersonRepositoryMock mockedRepository;

    @BeforeEach
    void setup() {
        this.classUnderTest = new PersonService(new PersonRepositoryMock())
    }

    @Test
    @DisplayName("minors() returns expected values and invocates findAll() 1 time")
    void doesWorkOnlyWithMock() {
        this.mockedRepository.setExpectedReadAllInvocations(1);
        Assertions.assertEquals(this.classUnderTest.minors().size(), 3);
        this.mockedRepository.verify();
    }

}
