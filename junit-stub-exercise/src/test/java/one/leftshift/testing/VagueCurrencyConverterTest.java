package one.leftshift.testing;

import one.leftshift.testing.common.model.Currency;
import one.leftshift.testing.common.util.Tuple;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * TODO: create a stub for {@link ExchangeRateProvider}
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public class VagueCurrencyConverterTest {

    private CurrencyConverter classUnderTest;

    @BeforeEach
    public void setup() {
        classUnderTest = new VagueCurrencyConverter();
    }

    @Test
    @DisplayName("Throws exception if supplied with below zero values")
    void testException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            classUnderTest.convertFromUSD(new BigDecimal("-1.2"));
        });
    }

    @ParameterizedTest
    @MethodSource("testValues")
    void testConvertsCurrenciesAsExpected(Tuple<BigDecimal, BigDecimal> testData) {
        BigDecimal result = classUnderTest.convertFromUSD(testData.getLeft());
        Assertions.assertEquals(testData.getRight(), result);
    }

    private static List<Tuple<BigDecimal, BigDecimal>> testValues() {
        return Arrays.asList(
                                              // input              expectation
                Tuple.of(BigDecimal.ONE, new BigDecimal("0.8523")),
                Tuple.of(new BigDecimal("55.50"), new BigDecimal("47.31")),
                Tuple.of(new BigDecimal("1000"), new BigDecimal("852.3"))
        );
    }
}
