package one.leftshift.testing;

import one.leftshift.testing.common.model.Currency;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public class VagueCurrencyConverter implements CurrencyConverter {

    private final ExchangeRateProvider exchangeRateProvider;

    public VagueCurrencyConverter(ExchangeRateProvider exchangeRateProvider) {
        this.exchangeRateProvider = exchangeRateProvider;
    }

    @Override
    public BigDecimal convertFromUSD(BigDecimal amountInUSD) {
        if (isBelowZero(amountInUSD)) {
            throw new IllegalArgumentException("Only positive amounts are allowed.");
        }
        final BigDecimal exchangeRate = exchangeRateProvider.provide(Currency.USD, Currency.EURO);

        return amountInUSD.multiply(exchangeRate, new MathContext(4, RoundingMode.UP));
    }

    private boolean isBelowZero(BigDecimal amountInUSD) {
        return amountInUSD.compareTo(BigDecimal.ZERO) < 0;
    }

}
