package one.leftshift.testing;

import one.leftshift.testing.common.model.Currency;

import java.math.BigDecimal;

/**
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public interface ExchangeRateProvider {
    BigDecimal provide(Currency from, Currency to);
}
