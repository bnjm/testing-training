package one.leftshift.testing;

import java.math.BigDecimal;

/**
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public interface CurrencyConverter {
    BigDecimal convertFromUSD(BigDecimal amountInUSD);
}
