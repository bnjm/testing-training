package one.leftshift.testing;

import one.leftshift.testing.common.model.Person;
import one.leftshift.testing.common.repository.PersonRepository;
import one.leftshift.testing.common.service.PersonService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * TODO: Implement a test spy for {@link PersonRepository} and delegate to {@link FakePersonRepository}
 *
 * @author benjamin.krenn@leftshift.one - 9/11/18.
 * @since 1.0.0
 */
public class PersonServiceTestWithSpy {

    private PersonService classUnderTest;
    private PersonRepositorySpy spy;
    private FakePersonRepository fakeRepository;

    @BeforeEach
    void setup() {
        fakeRepository = new FakePersonRepository();
        classUnderTest = new PersonService();
    }

    @Test
    @DisplayName("save is called twice")
    void testFindAllIsCalledOnce() {
        classUnderTest.save(Person.builder().withName().firstName("Hans").lastName("Peter").build());
        classUnderTest.save(Person.builder().withName().firstName("Frodo").lastName("Baggins").build());
        Assertions.assertThat(spy.saveInvocations).isEqualTo(2);
    }
}

    @Test
    @DisplayName("findAll is called once")
    void testFindAllIsCalledOnce() {
        classUnderTest.adults();
        Assertions.assertThat(spy.findAllInvocations).isEqualTo(1);
    }
}
