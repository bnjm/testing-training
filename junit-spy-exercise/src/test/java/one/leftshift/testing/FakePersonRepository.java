package one.leftshift.testing;

import one.leftshift.testing.common.model.Person;
import one.leftshift.testing.common.repository.PersonRepository;

import java.time.LocalDate;
import java.util.*;

/**
 * DO NOT TOUCH ME MY FRIENDS!
 *
 * @author benjamin.krenn@leftshift.one - 9/11/18.
 * @since 1.0.0
 */
public class FakePersonRepository implements PersonRepository {

    private final List<Person> store;

    public FakePersonRepository() {
        this.store = assemblePersons();
    }


    @Override
    public void create(Person person) {
        this.store.add(person);
    }

    @Override
    public Person read(UUID uuid) {
        return store.stream()
                .filter(p -> p.getUuid().equals(uuid))
                .findFirst().get();
    }

    @Override
    public Collection<Person> readAll() {
        return Collections.unmodifiableList(store);
    }

    @Override
    public void update(Person person) {
        throw new UnsupportedOperationException("No implemented");
    }

    @Override
    public boolean delete(Person person) {
        throw new UnsupportedOperationException("No implemented");
    }

    private List<Person> assemblePersons() {
        return Arrays.asList(
                Person.builder().withAge().dateOfBirth(LocalDate.of(1988,10,4)).withName().names("Peter", "Smith").build(),
                Person.builder().withAge().dateOfBirth(LocalDate.of(1996,10,4)).withName().names("Greta", "Johnson").build(),
                Person.builder().withAge().dateOfBirth(LocalDate.of(1978,10,4)).withName().names("Milos", "Kovacic").build(),
                Person.builder().withAge().dateOfBirth(LocalDate.of(1999,10,4)).withName().names("Gino", "Bruchetta").build(),
                Person.builder().withAge().dateOfBirth(LocalDate.of(1981,10,4)).withName().names("John", "Doe").build(),
                Person.builder().withAge().dateOfBirth(LocalDate.of(1997,10,4)).withName().names("Jane", "Stirling").build(),
                Person.builder().withAge().dateOfBirth(LocalDate.of(1991,10,4)).withName().names("Herman", "The German").build(),
                Person.builder().withAge().dateOfBirth(LocalDate.of(2013,10,4)).withName().names("Johannes", "Saint").build(),
                Person.builder().withAge().dateOfBirth(LocalDate.of(2002,10,4)).withName().names("Simon", "Clark").build(),
                Person.builder().withAge().dateOfBirth(LocalDate.of(1987,10,4)).withName().names("Mario", "Itzame").build()
        );
    }
}
