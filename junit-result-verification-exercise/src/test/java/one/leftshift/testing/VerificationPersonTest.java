package one.leftshift.testing;

import one.leftshift.testing.common.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

/**
 * TODO: refactor verification step
 *
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public class VerificationPersonTest {

    @Test
    @DisplayName("")
    void testCanBuyProduct() {
        //@formatter:off
        Person classUnderTest = Person.builder()
                .withName()
                    .firstName("Brian").lastName("Goetz")
                .withAddress()
                    .street("Downing Street 32").town("London").zipCode(2838)
                .withAge()
                    .dateOfBirth(LocalDate.of(1988, 10, 4))
                .build();
        //@formatter:on

        Assertions.assertEquals("Brian Goetz", classUnderTest.getName().getCompoundName());
        Assertions.assertEquals("Downing Street 32", classUnderTest.getAddress().getStreet());
        Assertions.assertEquals("London", classUnderTest.getAddress().getTown());
        Assertions.assertEquals(2838, classUnderTest.getAddress().getZipCode());
        Assertions.assertEquals(LocalDate.of(1988, 10, 4), classUnderTest.getAge().getDob());
    }

    private void assertPersonMeetsExpectation() {

    }
}
