package one.leftshift.testing.fixtures

import spock.lang.Issue
import spock.lang.Specification
import spock.lang.Subject

/**
 * A spock test class must extend {@link Specification}
 * @author benjamin.krenn@leftshift.one
 * @since 1.0.0
 */
@Issue("ABC-1337")
class SpockFixtures extends Specification {
    //  same as: @BeforeEach
    // run before every test method
    void setup() {
    
    }
    
    // same as: @BeforeAll
    // run before first test method
    void setupSpec() {
    
    }
    
    // same as: @AfterEach
    // run after every test method
    void cleanup() {
    
    }
    
    // same as: @AfterAll
    // run after the last test method
    void cleanupSpec() {
    
    }
    
    void "my cool test"() {
        given: //setup
            String someString = "abc"
        when: // exercise
            def result = someString.reverse()
        then: // verification
            result == "cba"
    }
    
}
