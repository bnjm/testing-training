package one.leftshift.testing;

import one.leftshift.testing.common.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

/**
 * TODO: refactor from inline fixture setup to delegated fixture setup
 *
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public class DelegatedFixtureSetupPersonTest {

    @Test
    @DisplayName("Person has the expected name")
    void testCanBuyProduct() {
        // setup
        //@formatter:off
        Person classUnderTest = Person.builder()
                .withName()
                    .firstName("Brian").lastName("Goetz")
                .withAddress()
                    .street("Downing Street 32").town("London").zipCode(2838)
                .withAge()
                    .dateOfBirth(LocalDate.of(1988, 10, 4))
                .build();
        //@formatter:on

        // exercise
        String result = classUnderTest.getName().getCompoundName();

        // verify
        Assertions.assertEquals("Brian Goetz", result);
    }


    /**
     * TODO: Answer this: This test might cause problems in the future. Why?
     */
    @Test
    @DisplayName("Person has the expected age")
    void testHasExpectedAge() {
        // setup
        //@formatter:off
        Person classUnderTest = Person.builder()
                .withName()
                    .firstName("Brian").lastName("Goetz")
                .withAddress()
                    .street("Downing Street 32").town("London").zipCode(2838)
                .withAge()
                    .dateOfBirth(LocalDate.of(1988, 10, 4))
                .build();
        //@formatter:on

        // exercise
        int result = classUnderTest.getAge().actualAge();

        // verify
        Assertions.assertEquals(29, result);
    }
}
