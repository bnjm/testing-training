package one.leftshift.testing;

import one.leftshift.testing.common.model.Person;
import one.leftshift.testing.common.service.PersonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

/**
 * TODO: implement a fake object that behaves like the real thing...
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public class PersonServiceWithFakeObjectTest {

    private PersonService classUnderTest;

    @BeforeEach
    void setup() {
        this.classUnderTest = new PersonService();
        populate();
    }

    @Test
    @DisplayName("there are 2 adults")
    void testTwoAdults() {
        Assertions.assertEquals(classUnderTest.adults().size(), 2);
    }

    @Test
    @DisplayName("there are 3 minors")
    void test3Minors() {
        Assertions.assertEquals(classUnderTest.minors().size(), 3);
    }

    @Test
    @DisplayName("total number of persons is 5")
    void testTotalPersonCount() {
        Assertions.assertEquals(classUnderTest.numberOfPersons(), 5);
    }

    @Test
    @DisplayName("all minors can be dumped")
    void testMinorsCanBeDumped() {
        classUnderTest.deleteAllMinors();
        Assertions.assertEquals(classUnderTest.minors().size(), 0);
    }

    private void populate() {
        this.classUnderTest.save(Person.builder().withAge().dateOfBirth(LocalDate.of(1999, 1, 1)).build());
        this.classUnderTest.save(Person.builder().withAge().dateOfBirth(LocalDate.of(2005, 1, 1)).build());
        this.classUnderTest.save(Person.builder().withAge().dateOfBirth(LocalDate.of(1988, 1, 1)).build());
        this.classUnderTest.save(Person.builder().withAge().dateOfBirth(LocalDate.of(1973, 1, 1)).build());
        this.classUnderTest.save(Person.builder().withAge().dateOfBirth(LocalDate.of(2001, 1, 1)).build());
    }

}
