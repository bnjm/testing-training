package one.leftshift.testing.common;

/**
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */

enum LogLevel {
    INFO
}

public interface Logger {
    void info(String message);
    LogLevel defaultLogLevel();
}

class LoggerStub implements Logger {
    @Override
    public void info(String message) {
        // do nothing
    }

    @Override
    public LogLevel defaultLogLevel() {
        return LogLevel.INFO;
    }
}


