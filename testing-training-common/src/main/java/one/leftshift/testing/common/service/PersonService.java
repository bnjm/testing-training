package one.leftshift.testing.common.service;

import one.leftshift.testing.common.model.Person;
import one.leftshift.testing.common.repository.PersonRepository;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A pretty useless service...
 *
 * @author benjamin.krenn@leftshift.one - 9/11/18.
 * @since 1.0.0
 */
public class PersonService {

    private final PersonRepository repository;

    public PersonService(PersonRepository repository) {
        this.repository = repository;
    }

    public void save(Person person) {
        this.repository.create(person);
    }

    public List<Person> adults() {
        return this.getMatchingPredicate(p -> p.getAge().actualAge() >= 21);
    }

    public List<Person> minors() {
        return this.getMatchingPredicate(p -> p.getAge().actualAge() < 21);
    }

    public void deleteAllMinors() {
        minors().forEach(this.repository::delete);
    }

    public int numberOfPersons() {
        return repository.readAll().size();
    }

    private List<Person> getMatchingPredicate(Predicate<Person> predicate) {
        return Collections.unmodifiableList(repository.readAll().stream()
                .filter(predicate).collect(Collectors.toList()));
    }
}
