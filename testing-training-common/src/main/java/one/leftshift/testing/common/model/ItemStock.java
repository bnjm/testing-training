package one.leftshift.testing.common.model;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class ItemStock {

    private int stock;

    public ItemStock(int stock) {
        this.stock = stock;
    }

    /**
     * Removes the given quantity from the stock and returns the count of removed items
     * @param quantity
     * @return count of removed items
     */
    public int remove(int quantity) {
        if (quantity > this.stock) {
            int removedItems = this.stock;
            this.stock = 0;
            return removedItems;
        }
        this.stock = this.stock - quantity;

        return quantity;
    }

    public boolean isEmpty() {
        if (this.stock == 0) {
            return true;
        }
        return false;
    }

    public void add(int quantity) {
        this.stock = this.stock + quantity;
    }
}
