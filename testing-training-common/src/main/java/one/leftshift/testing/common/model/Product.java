package one.leftshift.testing.common.model;

import java.util.Objects;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class Product {
    private final Price price;

    public Product(Price price) {
        this.price = price;
    }

    public Price getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {

        return Objects.hash(price);
    }
}
