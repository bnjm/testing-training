package one.leftshift.testing.common.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class CreditCard {
    private final Person owner;
    private final BigDecimal limit;

    public CreditCard(Person owner, BigDecimal limit) {
        this.owner = owner;
        this.limit = limit;
    }

    public Person getOwner() {
        return owner;
    }

    public BigDecimal getLimit() {
        return limit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditCard that = (CreditCard) o;
        return Objects.equals(owner, that.owner) &&
                Objects.equals(limit, that.limit);
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, limit);
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "owner=" + owner +
                ", limit=" + limit +
                '}';
    }
}
