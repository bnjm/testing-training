package one.leftshift.testing.common.model;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class CurrencyMismatchException extends RuntimeException {
    public CurrencyMismatchException() {
        super();
    }

    public CurrencyMismatchException(String message) {
        super(message);
    }

    public CurrencyMismatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public CurrencyMismatchException(Throwable cause) {
        super(cause);
    }

    protected CurrencyMismatchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
