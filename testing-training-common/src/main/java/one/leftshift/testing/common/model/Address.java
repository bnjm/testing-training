package one.leftshift.testing.common.model;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class Address {
    private final String street;
    private final String town;
    private final int zipCode;

    private Address(Builder builder) {
        this.street = builder.street;
        this.town = builder.town;
        this.zipCode = builder.zipCode;
    }

    public String getStreet() {
        return street;
    }

    public String getTown() {
        return town;
    }

    public int getZipCode() {
        return zipCode;
    }

    public static class Builder {
        private String street;
        private String town;
        private int zipCode;

        private Person.Builder parent;

        public Builder(Person.Builder parent) {
            this.parent = parent;
        }

        public Builder street(String street) {
            this.street = street;
            return this;
        }

        public Builder town(String town) {
            this.town = town;
            return this;
        }

        public Builder zipCode(int zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public Name.Builder withName() {
            return parent.withName();
        }

        public Address.Builder withAddress() {
            return parent.withAddress();
        }

        public Age.Builder withAge() {
            return parent.withAge();
        }

        public Address toBuild() {
            return new Address(this);
        }

        public Person build() {
            return this.parent.build();
        }

    }
}
