package one.leftshift.testing.common.model;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class Name {
    private final String firstName;
    private final String lastName;

    private Name(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastname;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCompoundName() {
        return firstName + " " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Name name = (Name) o;
        return Objects.equals(firstName, name.firstName) &&
                Objects.equals(lastName, name.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }


    public static class Builder {
        private String firstName;
        private String lastname;
        private Person.Builder parent;

        public Builder(Person.Builder parent) {
            this.parent = parent;
        }

        public Builder firstName(String firstName) {
            checkName(firstName);
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            checkName(lastName);
            this.lastname = lastName;
            return this;
        }

        public Builder names(String firstName, String lastname) {
            checkName(firstName, lastname);
            this.firstName = firstName;
            this.lastname = lastname;

            return this;
        }

        public Name.Builder withName() {
            return parent.withName();
        }

        public Address.Builder withAddress() {
            return parent.withAddress();
        }

        public Age.Builder withAge() {
            return parent.withAge();
        }

        public Name toBuild() {
            return new Name(this);
        }

        public Person build() {
            return this.parent.build();
        }

        private void checkName(String... possibleNames) {
            Arrays.stream(Objects.requireNonNull(possibleNames))
                    .forEach(possibleName -> {
                        if (possibleName == null || possibleName.isEmpty()) {
                            throw new RuntimeException("Invalid name");
                        }
                    });
        }
    }
}
