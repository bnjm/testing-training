package one.leftshift.testing.common.model;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public enum Currency {
    EURO,
    USD,
    GBP,
    CNY,
    JPY;

}
