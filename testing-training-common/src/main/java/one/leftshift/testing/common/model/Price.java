package one.leftshift.testing.common.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Price value object
 *
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class Price {
    private final BigDecimal price;
    private final Currency currency;

    public Price(BigDecimal price, Currency currency) {
        Objects.requireNonNull(price, "Price can not be null");
        Objects.requireNonNull(currency, "Currency can not be null");
        this.price = price;
        this.currency = currency;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Price add(Price other) {
        if (other.currency != this.currency) {
            throw new CurrencyMismatchException("");
        } else {
            return new Price(this.price.add(other.price), this.currency);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price1 = (Price) o;
        return Objects.equals(price, price1.price) &&
                currency == price1.currency;
    }

    @Override
    public int hashCode() {

        return Objects.hash(price, currency);
    }
}
