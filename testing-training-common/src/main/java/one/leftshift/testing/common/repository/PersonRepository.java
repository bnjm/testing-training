package one.leftshift.testing.common.repository;

import one.leftshift.testing.common.model.Person;

import java.util.Collection;
import java.util.UUID;

/**
 * Example repository for mocking CRUD operations.
 *
 * @author benjamin.krenn@leftshift.one - 9/11/18.
 * @since 1.0.0
 */
public interface PersonRepository {

    void create(Person person);

    Person read(UUID uuid);

    Collection<Person> readAll();

    void update(Person person);

    boolean delete(Person person);

}
