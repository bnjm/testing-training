package one.leftshift.testing.common.model;

import one.leftshift.testing.common.util.Tuple;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class ProductShop {

    private final BigDecimal productPrice = new BigDecimal("120.50");
    private final ItemStock stock = new ItemStock(100);


    public Tuple<List<Product>, Payment> buy(int quantity, CreditCard creditCard) {
        checkQuantity(quantity);
        final BigDecimal totalPrice = productPrice.multiply(new BigDecimal(quantity));
        checkCreditCardLimit(totalPrice, creditCard);

        int removedItems = this.stock.remove(quantity);

        List<Product> products = IntStream.rangeClosed(1, removedItems)
                .mapToObj(i -> new Product(new Price(productPrice, Currency.EURO)))
                .collect(Collectors.toList());

        return Tuple.of(products, new Payment(creditCard, new Price(totalPrice, Currency.EURO)));
    }

    private static void checkCreditCardLimit(BigDecimal totalPrice, CreditCard creditCard) {
        if (creditCard.getLimit().compareTo(totalPrice) < 0) {
            throw new RuntimeException("Creditcard limit is not sufficient (price: " + totalPrice + ")");
        }
    }

    private static void checkQuantity(int quantity) {
        if (quantity <= 0) {
            throw new InvalidQuantityException("Invalid quantity: " + quantity);
        }
    }


    public static class InvalidQuantityException extends RuntimeException {
        public InvalidQuantityException(String message) {
            super(message);
        }
    }
}
