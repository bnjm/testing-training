package one.leftshift.testing.common.model;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class Payment {
    private final CreditCard creditCard;
    private final Price price;

    public Payment(CreditCard creditCard, Price price) {
        this.creditCard = creditCard;
        this.price = price;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public Price getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "creditCard=" + creditCard +
                ", price=" + price +
                '}';
    }
}
