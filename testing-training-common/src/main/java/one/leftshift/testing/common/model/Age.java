package one.leftshift.testing.common.model;

import java.time.LocalDate;
import java.time.Period;

/**
 * @author benjamin.krenn@leftshift.one - 8/5/18.
 * @since 1.0.0
 */
public class Age {
    private final LocalDate dob;
    private final int age;

    private Age(Builder builder) {
        this.dob = builder.dob;
        this.age = calculateAge(builder.dob);
    }

    public LocalDate getDob() {
        return dob;
    }

    public int actualAge() {
        return age;
    }

    private int calculateAge(LocalDate date) {
        return Period.between(date, LocalDate.now()).getYears();
    }


    public static class Builder {
        private final Person.Builder parent;
        private LocalDate dob;

        public Builder(Person.Builder parent) {
            this.parent = parent;
        }

        public Builder dateOfBirth(LocalDate date) {
            this.dob = date;
            return this;
        }

        public Name.Builder withName() {
            return parent.withName();
        }

        public Address.Builder withAddress() {
            return parent.withAddress();
        }

        public Age.Builder withAge() {
            return parent.withAge();
        }

        public Age toBuild() {
            return new Age(this);
        }

        public Person build() {
            return this.parent.build();
        }
    }
}
