package one.leftshift.testing.common.model;

import java.util.UUID;

/**
 * @author benjamin.krenn@leftshift.one - 8/3/18.
 * @since 1.0.0
 */
public class Person {
    private final UUID uuid = UUID.randomUUID();
    private final Name name;
    private final Address address;
    private final Age age;

    private Person(Builder builder) {
        this.name = builder.nameBuilder.toBuild();
        this.address = builder.addressBuilder.toBuild();
        this.age = builder.ageBuilder.toBuild();
    }

    public Name getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public Age getAge() {
        return age;
    }

    public UUID getUuid() {
        return uuid;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Name.Builder nameBuilder = new Name.Builder(this);
        private Address.Builder addressBuilder = new Address.Builder(this);
        private Age.Builder ageBuilder = new Age.Builder(this);

        public Name.Builder withName() {
            return nameBuilder;
        }

        public Address.Builder withAddress() {

            return addressBuilder;
        }

        public Age.Builder withAge() {
            return ageBuilder;
        }

        public Person build() {
            return new Person(this);
        }
    }
}
