package one.leftshift.testing;

import one.leftshift.testing.common.model.Person;
import one.leftshift.testing.common.repository.PersonRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.UUID;

/**
 * TODO: Refactor into two parameterized test!
 *
 * see: https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests
 *
 * @author benjamin.krenn@leftshift.one - 9/11/18.
 * @since 1.0.0
 */
class ParameterizedWordUtilsTest {

    @Test
    @DisplayName("is word a palindrome?")
    void testIsWordPalindrome() {
        Assertions.assertThat(WordUtils.isPalindrom("peter")).isEqualTo(false);
        Assertions.assertThat(WordUtils.isPalindrom("madam")).isEqualTo(true);
        Assertions.assertThat(WordUtils.isPalindrom("abc")).isEqualTo(false);
        Assertions.assertThat(WordUtils.isPalindrom("dasd213dsad")).isEqualTo(false);
        Assertions.assertThat(WordUtils.isPalindrom("12321")).isEqualTo(true);
        Assertions.assertThat(WordUtils.isPalindrom("racecar")).isEqualTo(true);
    }
}