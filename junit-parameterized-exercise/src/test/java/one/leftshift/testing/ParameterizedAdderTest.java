package one.leftshift.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;

/**
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public class ParameterizedAdderTest {


    private static Adder adder;

    @BeforeAll
    static void setupSpec() {
        adder = new Adder();
    }

    @Test
    @DisplayName("Adds 1+1 as expected")
    void testAddOneAndOne() {
        int result = adder.add(1, 1);
        Assertions.assertEquals(2, result);
    }

    @Test
    @DisplayName("Adds 2+2 as expected")
    void testAddTwoAndTwo() {
        int result = adder.add(2, 2);
        Assertions.assertEquals(4, result);
    }

    @Test
    @DisplayName("Adds 1000 + 500 as expected")
    void testAddThousandAndFiveHundred() {
        int result = adder.add(1000, 500);
        Assertions.assertEquals(1500, result);
    }


    @ParameterizedTest
    @MethodSource("intSource")
    void testAddParameterized(List<Integer> testData) {
        Assertions.assertEquals(adder.add(testData.get(0), testData.get(1)), testData.get(2).intValue());
    }

    /**
     * A method source can return anything that can be reliably converted into a {@link java.util.stream.Stream} by junit.
     * @return
     */
    private static List<List<Integer>> intSource() {
        return Arrays.asList(
                           // a     b       result  [datatable]
                Arrays.asList(1,    1,      2),
                Arrays.asList(2,    2,      4),
                Arrays.asList(1000, 500,    1500)
        );
    }
}
