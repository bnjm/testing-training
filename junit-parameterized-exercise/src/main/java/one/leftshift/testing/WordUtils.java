package one.leftshift.testing;

import java.util.Arrays;

/**
 * @author benjamin.krenn@leftshift.one - 9/11/18.
 * @since 1.0.0
 */
public final class WordUtils {

    /**
     * Checks if a word or a sequence of numbers given in its string representation is a palindrome.
     * A word is a palindrome when it reads backwards the same as forwards.
     * e.g: "madam" is a palindrome.
     *
     * @param word
     * @return
     */
    public static boolean isPalindrom(String word) {
        char[] chars = word.toCharArray();
        char[] reversed = reverse(chars);

        return Arrays.equals(chars, reversed);
    }

    private static char[] reverse(char[] chars) {
        char[] reversed = new char[chars.length];

        for (int i = chars.length - 1; i >= 0; i--) {
            reversed[(chars.length - 1) - i] = chars[i];
        }
        return reversed;
    }
}
