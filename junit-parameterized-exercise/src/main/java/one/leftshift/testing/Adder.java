package one.leftshift.testing;

/**
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public class Adder {

    public int add(int a, int b) {
        return Math.addExact(a, b);
    }
}
