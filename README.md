![logo](http://www.leftshift.one/wp-content/uploads/2017/04/hero-logo-1.png)

## Testing Basics Training

> I find that writing unit tests actually increases my programming speed

_Martin Fowler_


> I get paid for code that works, not for tests, so my philosophy is to test as little as possible to reach a given level of confidence

_Kent Beck (Creator of JUnit)_

> Never in the annals of software engineering was so much owed by so many to so few lines of code

_Martin Fowler on JUnit_

### Agenda
> to be defined