package one.leftshift.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * TODO: fix this test using implicit fixture hooks (@AfterEach)
 *
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public class PersistentFixtureStudentTest {

    @Test
    @DisplayName("Student has expected values")
    void testStudentHasExpectedValues() {
        Student classUnderTest = new Student("Doug Lea", 38);
        Assertions.assertEquals(1, classUnderTest.getId());
        Assertions.assertEquals("Doug Lea", classUnderTest.getName());
        Assertions.assertEquals(38, classUnderTest.getAge());
    }

    @Test
    @DisplayName("toString() returns expected string")
    void testToStringOnStudent() {
        Student classUnderTest = new Student("Robert C. Martin", 52);
        Assertions.assertEquals("Student(id: 1, name: Robert C. Martin, age: 52)", classUnderTest.toString());
    }
}
