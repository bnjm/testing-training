package one.leftshift.testing;

/**
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public final class StatefulLongSequence {

    private final static long INITIAL = 1;
    private static long sequenceNumber = INITIAL;

    private StatefulLongSequence() {
        throw new UnsupportedOperationException("can not instantiate " + StatefulLongSequence.class.getSimpleName());
    }

    public synchronized static long next() {
        return sequenceNumber++;
    }

    public synchronized static void reset() {
        StatefulLongSequence.sequenceNumber = INITIAL;
    }
}
