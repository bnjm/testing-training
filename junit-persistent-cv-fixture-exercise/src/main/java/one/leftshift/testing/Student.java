package one.leftshift.testing;

import java.text.MessageFormat;

/**
 * @author benjamin.krenn@leftshift.one - 9/23/18.
 * @since 1.0.0
 */
public class Student {
    private final long id; // from sequence
    private final String name;
    private final int age;

    public Student(String name, int age) {
        this.id = StatefulLongSequence.next();
        this.name = name;
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return MessageFormat.format("Student(id: {0}, name: {1}, age: {2})", id, name, age);
    }
}
